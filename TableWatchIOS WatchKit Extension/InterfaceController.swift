//
//  InterfaceController.swift
//  TableWatchIOS WatchKit Extension
//
//  Created by Sukhwinder Rana on 2019-06-20.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    @IBOutlet weak var tableWatch: WKInterfaceTable!
    var Pokemon:[String] = ["pikachu", "squirtle", "raichu", "jigglypuff", "meon"]
    var PokImage:[String] = ["pikachu","suirtle", "raichu","jigglypuff", "meon"]
   
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        print("heloo")
         self.tableWatch.setNumberOfRows(self.Pokemon.count, withRowType: "sukh")
        
        for (index , name) in Pokemon.enumerated(){
            let row = self.tableWatch.rowController(at: index) as! WatchViewController
            row.personLabel.setText(name)
        
        }
        for (index , name) in PokImage.enumerated(){
            let row = self.tableWatch.rowController(at: index) as! WatchViewController
            row.pikachuPic.setImageNamed(name)
            
        }
       
        
    
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
