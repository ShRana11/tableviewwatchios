//
//  WatchViewController.swift
//  TableWatchIOS WatchKit Extension
//
//  Created by Sukhwinder Rana on 2019-06-20.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import WatchKit

class WatchViewController: NSObject {
    
    @IBOutlet weak var personLabel: WKInterfaceLabel!
    
    @IBOutlet weak var pikachuPic: WKInterfaceImage!
}
